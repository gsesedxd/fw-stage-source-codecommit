#!/usr/bin/env node
import 'source-map-support/register';
import cdk = require('@aws-cdk/core');
import { FwStageSourceCodeCommitStack } from '../lib/fw-stage-source-code_commit-stack';

const app = new cdk.App();
new FwStageSourceCodeCommitStack(app, 'FwStageSourceCodeCommitStack');
