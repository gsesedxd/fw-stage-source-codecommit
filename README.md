# Useful commands

 * `npm run build`   compile typescript to js
 * `npm run watch`   watch for changes and compile
 * `npm run test`    perform the jest unit tests
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template


## Usage
1.- Install the AWS CDK using the following command.
`npm install -g aws-cdk`

* Run the following command to see the version number of the AWS CDK.
cdk --version

2.- Create a folder from save the resource
```sh
mkdir FW-Stage-moduleType-moduleName
```

3.- To initialize your new AWS CDK app inside a project folder(first create a folder with mkdir): `cdk init --language LANGUAGE [TEMPLATE]`
```sh
cdk init --language typescript
```

If you get an error message that your language framework is out of date, use one of the following commands to update the components that the AWS CDK needs to support the language.

`npx npm-check-updates -u`

### INFO
[AWS-CDK Stacks](https://docs.aws.amazon.com/cdk/latest/guide/stacks.html)

## Modules List

### Pipeline
* `PipelineTemplateBase.json`

### Source
* FW-Stage-Source-Queue (`SourceQueue.json`)
* FW-Stage-Source-CodeCommit (`SourceCodeCommit.json`)
* FW-Stage-Source-S3

### Build
* FW-Stage-Build-BuilderCustom (`BuilderBuildSpecCustom.json`)
* FW-Stage-Build-Custom (`BuilderCustom.json`)
* FW-Stage-Build-Lambda (`BuilderLambda.json`)
* FW-Stage-Build-MBaaS (`BuilderMBaaS.json`) 
* FW-Stage-Build-WebAdvanced (`BuilderWebAdvanced.json`) 
* FW-Stage-Build-WebBasic (`BuilderWebBasic.json`)

### Deploy
* FW-Stage-Deploy-WebBasic (`DeployerWebBasic.json`) 
* FW-Stage-Deploy-WebAdvanced (`DeployerWebAdvanced.json`)
* FW-Stage-Deploy-CF (`DeployerCF.json`)
* FW-Stage-Deploy-CFBasic
* FW-Stage-Deploy-CFCrossAccount*
* FW-Stage-Deploy-CodeDeploy

### Test
* FW-Stage-Test-Selenium
* FW-Stage-Test-SonarQube
* FW-Stage-Test-TestAB
* FW-Stage-Test-CFBasic
* FW-Stage-Test-CFAdvanced
* FW-Stage-Test-CFMirror
* FW-Stage-Test-WebBasic
* FW-Stage-Test-WebAdvanced

### Communication
* FW-Stage-Communication-Simple
* FW-Stage-Communication-Aproval