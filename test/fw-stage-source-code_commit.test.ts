import { expect as expectCDK, matchTemplate, MatchStyle } from '@aws-cdk/assert';
import cdk = require('@aws-cdk/core');
import FwStageSourceCodeCommit = require('../lib/fw-stage-source-code_commit-stack');

test('Empty Stack', () => {
    const app = new cdk.App();
    // WHEN
    const stack = new FwStageSourceCodeCommit.FwStageSourceCodeCommitStack(app, 'MyTestStack');
    // THEN
    expectCDK(stack).to(matchTemplate({
      "Resources": {}
    }, MatchStyle.EXACT))
});