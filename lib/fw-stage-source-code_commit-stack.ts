import cdk = require('@aws-cdk/core');
import codecommit = require('@aws-cdk/aws-codecommit');
import codepipeline_actions = require('@aws-cdk/aws-codepipeline-actions');
import codepipeline = require('@aws-cdk/aws-codepipeline');

// @WIP: Importar de otra manera
import { Constants } from './../../FW-CoreLib/lib/constants';
import { Category, Owner, Provider } from './../../fw-corelib/lib/configuration';

export class FwStageSourceCodeCommitStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Resources
    // CodeCommit
    const CodeCommit = new codecommit.CfnRepository(this, 'CodeCommit', {
      repositoryName: `${Constants.IDProject}-${Constants.EnvProject}-sourcecodecommit`
    });

    // Outputs
    new cdk.CfnOutput(this, 'SourceCodeCommit', {
      value: CodeCommit.attrName
    });
  }

  getConfig() {
    return {
      name: 'Src',
      actions: [
        {
          actionTypeId: {
            category: Category.APPROVAL,
            owner: Owner.AWS,
            provider: Provider.S3,
            version: '1'
          },
          name: 'sdf',
          inputArtifacts: [
            {
              name: 'inputArtifact'
            }
          ],
          outputArtifacts: [
            {
              name: 'outputArtifact'
            }
          ],
          runOrder: 1,
          configuration: {
            S3Bucekt: 'bucket',
            S3Key: 'fileName.json'
          }
        }
      ],
    }
  }
}
